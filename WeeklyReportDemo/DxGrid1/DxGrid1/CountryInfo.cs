using System;


namespace DxGrid1 {
  public class CountryInfo {
    public CountryInfo( ) {

    }
    public CountryInfo(string name, int areaKM2, int population) {
      this.name = name;
      this.areaKM2 = areaKM2;
      this.population = population;
    }
    private string name;
    public string Name {
      get {
        return name;
      }
      set {
        name = value;
      }
    }
    private int areaKM2;
    public int AreaKM2 {
      get {
        return areaKM2;
      }
      set {
        areaKM2 = value;
      }
    }
    private int population;
    public int Population {
      get {
        return population;
      }
      set {
        population = value;
      }
    }
    public override string ToString( ) {
      return String.Format("Country: {0}, Area km^2: {1}, Population: {2}",
        name, areaKM2, population);
    }
  }
}