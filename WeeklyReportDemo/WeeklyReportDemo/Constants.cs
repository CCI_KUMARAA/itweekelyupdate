﻿/*
Class Name        : Constants
Class Description : Constants contains the constant values.
Author            : Sneha
Date created      : 01 June 2016
Date modified     : 20 Feb 2017
*/

namespace WeeklyReportDemo
{
    /// <summary>
    /// Constants file for constant values.
    /// </summary>
    public static class Constants
    {
        public const string DASHBOARD_SHEET = "Dashboard";
        public const string ALL_OPEN_ITEMS_SHEET = "All Open Items";
        public const string COMPLETED_ITEMS_LAST_30_DAYS_SHEET = "Completed Items Last 30 Days";
        public const string NEW_ITEMS_LAST_7_DAYS_SHEET = "New Items Last 7 Days";
        public const string COMPLETED_ITEMS_QUARTER_SHEET = "Completed Items Q{0} 2016";
        public const string STAKE_HOLDER = "Custom field (Stakeholder)";//"Stake Holder";// "Sponsor";//"Custom field (Stakeholder)";//// 
        public const string PRIORITY = "Priority";
    }
}
