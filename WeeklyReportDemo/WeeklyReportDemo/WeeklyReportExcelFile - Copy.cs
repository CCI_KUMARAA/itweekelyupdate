﻿/*
Class Name        : WeeklyReportExcelFile
Class Description : WeeklyReportExcelFile class creates an excel file.
Author            : Sneha
Date created      : 01 June 2016
Date modified     : 01 June 2016
*/

//using Microsoft.Office.Interop.Excel;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
//using Excel = Microsoft.Office.Interop.Excel;

using Microsoft.Office.Interop.Excel;

namespace WeeklyReportDemo
{
    /// <summary>
    /// WeeklyReportExcelFile class creates an excel file.
    /// </summary>
    public class WeeklyReportExcelFile 
    {
        Microsoft.Office.Interop.Excel.Application excelApplication;
        Excel.Workbook workBook = null;
        Excel.Worksheet sheet1 = null;
        Excel.Worksheet sheet2 = null;
        Excel.Worksheet sheet3 = null;
        Excel.Worksheet sheet4 = null;
        Excel.Worksheet sheet5 = null;
        string fileDetails = string.Empty;

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public WeeklyReportExcelFile()
        {
            // Creating excel document.
            CreateExcelDocument();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates the excel document
        /// </summary>
        public void CreateExcelDocument()
        {
            try
            {
                excelApplication = new Excel.Application();
                //excelApplication.Visible = true;
                workBook = excelApplication.Workbooks.Add();
                // Add worksheets
                AddWorkSheets();
               
                // Save the excel file.
                SaveExcelFile();

                // Copy the data from the respective excel files to the repesctive sheets.
                CopyDataToWorkbook();

                #region MyRegion

                //{
                //    //Choose DataRange
                //    CellRange dataRange = sheet.Range[1, 1, lastRow, lastCol];
                //    PivotCache cache = workbook.PivotCaches.Add(dataRange);
                //    PivotTable pt = report.PivotTables.Add("report", sheet.Range["A1"], cache);

                //    //Set Row Labels
                //    var r1 = pt.PivotFields["VendorNo"];
                //    r1.Axis = AxisTypes.Row;
                //    pt.Options.RowHeaderCaption = "VendorNo";

                //    var r2 = pt.PivotFields["Description"];
                //    r2.Axis = AxisTypes.Row;

                //    //Add Pivot Fields
                //    pt.DataFields.Add(pt.PivotFields["OnHand"], "SUM of OnHand", SubtotalTypes.Sum);
                //    pt.DataFields.Add(pt.PivotFields["OnOrder"], "SUM of OnOrder", SubtotalTypes.Sum);
                //    pt.DataFields.Add(pt.PivotFields["Cost"], "SUM of Cost", SubtotalTypes.Sum);
                //    pt.DataFields.Add(pt.PivotFields["ListPrice"], "Average of ListPrice", SubtotalTypes.Average);

                //    //Set Style
                //    pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleMedium6;
                //}

                //  Excel.Workbook activeWorkBook = null;
                //  Excel.Sheets sheets = null;
                //  Excel.Worksheet pivotWorkSheet = null;
                //  Excel.PivotCaches pivotCaches = null;
                //  Excel.PivotCache pivotCache = null;
                //  Excel.PivotTable pivotTable = null;
                //  Excel.PivotFields pivotFields = null;
                //  Excel.PivotField monthPivotField = null;
                //  Excel.PivotField productPivotField = null;
                //  Excel.PivotField salesPersonPivotField = null;
                //  Excel.PivotField unitsSoldPivotField = null;
                //  Excel.PivotField unitsSoldSumPivotField = null;


                //  activeWorkBook = excelApplication.ActiveWorkbook;
                //  sheets = excelApplication.Sheets;
                //  pivotWorkSheet = (Excel.Worksheet)sheets.Add();

                //  // Create the Pivot Table
                //  pivotCaches = activeWorkBook.PivotCaches();
                //  pivotCache = pivotCaches.Create(Excel.XlPivotTableSourceType.xlDatabase,
                //      "All Open Items!A4:O7");
                ////  pivotTable = pivotCache.CreatePivotTable(TableDestination: "All Open Items!A4:O2471");
                //  Excel.PivotTables pivotTables = (Excel.PivotTables)pivotWorkSheet.PivotTables();

                //  pivotTable = pivotTables.Add(pivotCache, excelApplication.ActiveCell);

                //  // Set the Pivot Fields
                //  pivotFields = (Excel.PivotFields)pivotTable.PivotFields();

                //  // Month Pivot Field
                //  monthPivotField = (Excel.PivotField)pivotFields.Item("Month");
                //  monthPivotField.Orientation = Excel.XlPivotFieldOrientation.xlRowField;
                //  monthPivotField.Position = 1;

                //  // Product Pivot Field
                //  productPivotField = (Excel.PivotField)pivotFields.Item("Product");
                //  productPivotField.Orientation = Excel.XlPivotFieldOrientation.xlColumnField;

                //  // Sales Person Pivot Field
                //  salesPersonPivotField = (Excel.PivotField)pivotFields.Item("SalesPerson");
                //  salesPersonPivotField.Orientation = Excel.XlPivotFieldOrientation.xlPageField;

                //  // Units Sold Pivot Field
                //  unitsSoldPivotField = (Excel.PivotField)pivotFields.Item("Units Sold");

                //  // Sum of Units Sold Field
                //  unitsSoldSumPivotField = pivotTable.AddDataField(unitsSoldPivotField,
                //      "# Units Sold", Excel.XlConsolidationFunction.xlSum);


                //Excel.Workbook book = excelApplication.ActiveWorkbook;
                //Excel.Worksheet sheet = book.Worksheets[1] as Excel.Worksheet;
                //Excel.PivotCaches pCaches = book.PivotCaches();
                //Excel.PivotCache pCache = pCaches.Create(Excel.XlPivotTableSourceType.xlDatabase, "All Open Items!A4:O2471", Excel.XlPivotTableVersionList.xlPivotTableVersion14);
                //Excel.Range rngDes = sheet.get_Range("C1");
                ////Excel.PivotTable pTable = pCache.CreatePivotTable(TableDestination: rngDes);
                //Excel.PivotTables pivotTables = (Excel.PivotTables)sheet.PivotTables();

                //Excel.PivotTable pivotTable = pivotTables.Add(pCache, excelApplication.ActiveCell, TableName: "PivotTable1");

                //Excel.Workbook book = excelApplication.ActiveWorkbook;
                //Excel.Worksheet sheet = book.Worksheets[1] as Excel.Worksheet;
                //Excel.PivotCaches pCaches = book.PivotCaches();
                //Excel.PivotCache pCache = pCaches.Create(Excel.XlPivotTableSourceType.xlDatabase, "All Open Items!A4:O2471", Excel.XlPivotTableVersionList.xlPivotTableVersion14);
                //Excel.Range rngDes = sheet.get_Range("C1");
                //Excel.PivotTable pTable = pCache.CreatePivotTable(TableDestination: rngDes, TableName: "PivotTable1", DefaultVersion: Excel.XlPivotTableVersionList.xlPivotTableVersion14);
                //Excel.PivotField fieldQ = pTable.PivotFields("Question");
                //Excel.PivotField fieldA = pTable.PivotFields("Answer");

                //fieldQ.Orientation = Excel.XlPivotFieldOrientation.xlRowField;
                //fieldQ.Position = 1;
                //fieldA.Orientation = Excel.XlPivotFieldOrientation.xlRowField;
                //fieldA.Position = 2; 
                #endregion

                // Pivot the sheets.
               
                // Format the sheets.

                

                // Mail the workbook.


                //workSheet.Cells[1, "A"].Value2 = "SNO";
                //workSheet.Cells[2, "B"].Value2 = "A";
                //workSheet.Cells[2, "C"].Value2 = "1122";

                //workBook.Close(true);
                //excelApplication.Quit();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Copies the data from the downloaded excels to the current workbook.
        /// </summary>
        private void CopyDataToWorkbook()
        {
            #region Old Method
            //All+Open+Issues+%28CCI+Project+Portal%29.xls
            // Excel.Workbook sourceWorkbook = excelApplication.Workbooks.Open(@"C:\Users\kashas\Downloads\_All+Open+Items+%28CCI+Project+Portal%29 (1).xls");
            // Excel.Worksheet sourceWorkSheet = sourceWorkbook.Sheets[1];
            //// Excel.Workbook destinationWorkbook = excelApplication.Workbooks.Open(@"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\All+Open+Issues+%28CCI+Project+Portal%29.xls");
            // workBook.Worksheets.Add(sourceWorkbook.Worksheets[1]);
            // workBook.Sheets.Copy(sourceWorkbook.Sheets[1]);

            //string pathFileDestination = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\ IT Weekly Update 2016 6 1.xlsx";
            //Excel.Application excel = new Excel.Application();
            ////excel.Visible = true;
            //Excel.Workbook wbDest = excel.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
            //Excel.Worksheet WorksheetDest = wbDest.Sheets[2];
            ////Clear all contents in Destination workbook
            //WorksheetDest.UsedRange.ClearContents();
            //// WorksheetDest.get_Range("A1").Value = "No Data";
            //wbDest.Save();
            //wbDest.Close();

            //Open the Source file
            //Excel.Workbook wbSource = excel.Workbooks.Open(@"C:\Users\kashas\Downloads\_All+Open+Items+%28CCI+Project+Portal%29 (1).xls", 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
            //Excel.Worksheet WorksheetSource = wbSource.Sheets[1];
            ////_Completed+Items+Last+30+Days+%28CCI+Project+Portal%29 (1).xls
            ////Copy all range in this worksheet
            //WorksheetSource.UsedRange.Copy(Missing.Value);
            ////Open destination workbook
            ////Excel.Workbook wbDestination = excel.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
            //Excel.Workbook wbDestination = excel.Workbooks.Open(pathFileDestination);
            //Excel.Worksheet WorksheetDestination = wbDestination.Sheets[2];
            //WorksheetDestination.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone);
            ////WorksheetDestination.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, misValue, misValue);
            //wbDestination.Save();
            //wbSource.Close();

            ////Open the Source file
            //Excel.Workbook wbSource2 = excel.Workbooks.Open(@"C:\Users\kashas\Downloads\_Completed+Items+Last+30+Days+%28CCI+Project+Portal%29 (1).xls", 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
            //Excel.Worksheet WorksheetSource2 = wbSource2.Sheets[1];
            ////_Completed+Items+Last+30+Days+%28CCI+Project+Portal%29 (1).xls
            ////Copy all range in this worksheet
            //WorksheetSource2.UsedRange.Copy(Missing.Value);
            ////Open destination workbook
            ////Excel.Workbook wbDestination = excel.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
            //Excel.Workbook wbDestination2= excel.Workbooks.Open(pathFileDestination);
            //Excel.Worksheet WorksheetDestination2 = wbDestination2.Sheets[3];
            //WorksheetDestination2.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone);
            ////WorksheetDestination.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, misValue, misValue);
            //wbDestination.Save();
            //wbSource2.Close(); 
            #endregion

            try
            {
                //string pathFileDestination = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\ IT Weekly Update 2016 6 1.xlsx";
                string pathFileDestination = fileDetails;
                //Excel.Application excel = new Excel.Application();
                Excel.Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Excel.Worksheet WorksheetDest = wbDest.Sheets[2];
                //Clear all contents in Destination workbook
                WorksheetDest.UsedRange.ClearContents();
                wbDest.Save();
                wbDest.Close();

                //Open the Source file
                Excel.Workbook wbSource = excelApplication.Workbooks.Open(@"C:\Users\kashas\Downloads\_All+Open+Items+%28CCI+Project+Portal%29 (1).xls", 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Excel.Worksheet WorksheetSource = wbSource.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                Excel.Workbook wbDestination = excelApplication.Workbooks.Open(pathFileDestination);
                Excel.Worksheet WorksheetDestination = wbDestination.Sheets[2];
                WorksheetDestination.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
                wbSource.Close();

                //Open the Source file //C:\Users\kashas\Downloads\_Completed+Items+Last+30+Days+%28CCI+Project+Portal%29 (1).xls
                Excel.Workbook wbSource2 = excelApplication.Workbooks.Open(@"C:\Users\kashas\Downloads\_Completed+Items+Last+30+Days+%28CCI+Project+Portal%29 (1).xls", 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Excel.Worksheet WorksheetSource2 = wbSource2.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource2.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                //Excel.Workbook wbDestination2= excel.Workbooks.Open(pathFileDestination);
                Excel.Worksheet WorksheetDestination2 = wbDestination.Sheets[3];
                WorksheetDestination2.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
                //wbSource2.Close();

                Excel.Range range = WorksheetDestination2.get_Range("A1", "C7");
                Excel.PivotCaches cache = wbSource2.PivotCaches();
                cache.Creator
                Excel.PivotTable pt = report.PivotTables.Add("report", sheet.Range["A1"], cache);
                //PivotCache cache = wbSource2.PivotCaches.Add(dataRange);
                //PivotTable pt = report.PivotTables.Add("report", sheet.Range["A1"], cache);

                wbDestination.Save();
                wbSource2.Close();

                //Open the Source file
                Excel.Workbook wbSource3 = excelApplication.Workbooks.Open(@"C:\Users\kashas\Downloads\_New+Items+Last+7+Days+%28CCI+Project+Portal%29 (1).xls", 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Excel.Worksheet WorksheetSource3 = wbSource3.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource3.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                //Excel.Workbook wbDestination3 = excel.Workbooks.Open(pathFileDestination);
                Excel.Worksheet WorksheetDestination3 = wbDestination.Sheets[4];
                WorksheetDestination3.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
                wbSource3.Close();

                //Open the Source file
                Excel.Workbook wbSource4 = excelApplication.Workbooks.Open(@"C:\Users\kashas\Downloads\_Completed+Items+Q2+2016+%28CCI+Project+Portal%29 (1).xls", 0, false, 1, "", "", false, Excel.XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Excel.Worksheet WorksheetSource4 = wbSource4.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource4.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                //Excel.Workbook wbDestination4 = excel.Workbooks.Open(pathFileDestination);
                Excel.Worksheet WorksheetDestination4 = wbDestination.Sheets[5];
                WorksheetDestination4.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteAll, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
                wbSource4.Close();

                // Save the excel file.
                //SaveExcelFile();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Add the worksheets.
        /// </summary>
        private void AddWorkSheets()
        {
            try
            {
                var sheetCollection = new Microsoft.Office.Interop.Excel.Worksheet[5];
                sheetCollection[0] = workBook.Worksheets.Add();
                sheetCollection[0].Name = string.Format(Constants.COMPLETED_ITEMS_QUARTER_SHEET, 2);
                sheetCollection[1] = workBook.Worksheets.Add();
                sheetCollection[1].Name = Constants.NEW_ITEMS_LAST_7_DAYS_SHEET;
                sheetCollection[2] = workBook.Worksheets.Add();
                sheetCollection[2].Name = Constants.COMPLETED_ITEMS_LAST_30_DAYS_SHEET;
                sheetCollection[3] = workBook.Worksheets.Add();
                sheetCollection[3].Name = Constants.ALL_OPEN_ITEMS_SHEET;
                sheetCollection[4] = workBook.Worksheets.Add();
                sheetCollection[4].Name = Constants.DASHBOARD_SHEET;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Saves the excel file in the specified name format.
        /// </summary>
        private void SaveExcelFile()
        {
            try
            {
                string currentYear = DateTime.Now.Year.ToString();
                string currentMonth = DateTime.Now.Month.ToString();
                string currentDate = DateTime.Now.Day.ToString();
                var appSettings = ConfigurationManager.AppSettings;
                string filePath = appSettings["FilePath"];
                fileDetails = string.Format(@"{0} IT Weekly Update {1} {2} {3}.xlsx", filePath, currentYear, currentMonth, currentDate);
                this.excelApplication.ActiveWorkbook.SaveAs(fileDetails);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        //System.Data.DataTable Pivot(System.Data.DataTable dt, DataColumn pivotColumn, DataColumn pivotValue)
        //{
        //    // find primary key columns 
        //    //(i.e. everything but pivot column and pivot value)
        //    System.Data.DataTable temp = dt.Copy();
        //    temp.Columns.Remove(pivotColumn.ColumnName);
        //    temp.Columns.Remove(pivotValue.ColumnName);
        //    string[] pkColumnNames = temp.Columns.Cast<DataColumn>()
        //        .Select(c => c.ColumnName)
        //        .ToArray();

        //    // prep results table
        //    System.Data.DataTable result = temp.DefaultView.ToTable(true, pkColumnNames).Copy();
        //    result.PrimaryKey = result.Columns.Cast<DataColumn>().ToArray();
        //    dt.AsEnumerable()
        //        .Select(r => r[pivotColumn.ColumnName].ToString())
        //        .Distinct().ToList()
        //        .ForEach(c => result.Columns.Add(c, pivotColumn.DataType));

        //    // load it
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        // find row to update
        //        DataRow aggRow = result.Rows.Find(
        //            pkColumnNames
        //                .Select(c => row[c])
        //                .ToArray());
        //        // the aggregate used here is LATEST 
        //        // adjust the next line if you want (SUM, MAX, etc...)
        //        aggRow[row[pivotColumn.ColumnName].ToString()] = row[pivotValue.ColumnName];
        //    }

        //    return result;
        //}
        #endregion
    }
}
