﻿/*
Class Name        : WeeklyReportExcelFile
Class Description : WeeklyReportExcelFile class creates an excel file.
Author            : Sneha
Date created      : 01 June 2016
Date modified     : 20 Feb 2017
*/

using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Collections.Generic;

namespace WeeklyReportDemo
{
    /// <summary>
    /// WeeklyReportExcelFile class creates an excel file.
    /// </summary>
    public class WeeklyReportExcelFile
    {
        Application excelApplication;
        Workbook workBook = null;
        string fileDetails = string.Empty;
        int quarter;

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public WeeklyReportExcelFile()
        {
            // Get the current quarter.
            DateTime date = DateTime.Today;
            quarter = (date.Month - 1) / 3 + 1;
            // Creating excel document.
            CreateExcelDocument();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates the excel document
        /// </summary>
        public void CreateExcelDocument()
        {
            try
            {
                excelApplication = new Application();
                workBook = excelApplication.Workbooks.Add();
                // Add worksheets
                AddWorkSheets();

                // Save the excel file.
                SaveExcelFile();

                // Copy the data from the respective excel files to the repesctive sheets.
                CopyDataToWorkbook();

                // Pivot the sheets.    
               // GetPivot();
                //AddPivotTable();
                //// Format the sheets.
                //AddPivotTable2();

                NewCPV_2();
                // Mail the workbook.
               // SendMail();

                //workSheet.Cells[1, "A"].Value2 = "SNO";
                //workSheet.Cells[2, "B"].Value2 = "A";
                //workSheet.Cells[2, "C"].Value2 = "1122";

                //workBook.Close(true);
                //excelApplication.Quit();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Creates the Pivot table
        /// </summary>
        public void GetPivot()
        {
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            try
            {
                string fileName = fileDetails;
                string completedItems30Days = ConfigurationManager.AppSettings["CompletedItems30"];
                string newItemsLast7Days = ConfigurationManager.AppSettings["NewItemsLast7Days"];
                string completedItemsInQuarter = ConfigurationManager.AppSettings["CompletedItemsInQuarter"];
                string dashboard = ConfigurationManager.AppSettings["Dashboard"];

                workbook.LoadFromFile(fileName);

                Spire.Xls.Worksheet dashboardSheet = workbook.Worksheets[dashboard];
                //Spire.Xls.CellRange completedItems30DaysDataRange = completedItems30DaysSheet.Range["A4:M300"]; // original code statement
                //Spire.Xls.CellRange completedItems30DaysDataRange = completedItems30DaysSheet.Range[4, completedItems30DaysSheet.LastRow - 1];
                // Completed items 30 days.
                Spire.Xls.Worksheet completedItems30DaysSheet = workbook.Worksheets[completedItems30Days];
                Spire.Xls.CellRange completedItems30DaysDataRange = completedItems30DaysSheet.Range[1, completedItems30DaysSheet.FirstColumn,
                    completedItems30DaysSheet.LastRow - 1, completedItems30DaysSheet.LastColumn];
                Spire.Xls.PivotCache completedItems30DaysCache = workbook.PivotCaches.Add(completedItems30DaysDataRange);
                Spire.Xls.PivotTable completedItems30DaysPivotTable =
                    dashboardSheet.PivotTables.Add(Constants.COMPLETED_ITEMS_LAST_30_DAYS_SHEET, completedItems30DaysSheet.Range["A2"], completedItems30DaysCache);
                var r1 = completedItems30DaysPivotTable.PivotFields[Constants.STAKE_HOLDER];
                r1.Axis = Spire.Xls.AxisTypes.Row;
                completedItems30DaysPivotTable.Options.RowHeaderCaption = Constants.STAKE_HOLDER;
                var r2 = completedItems30DaysPivotTable.PivotFields[Constants.PRIORITY];
                r2.Axis = Spire.Xls.AxisTypes.Column;
                completedItems30DaysPivotTable.Options.ColumnHeaderCaption = Constants.PRIORITY;
                completedItems30DaysPivotTable.DataFields.Add(completedItems30DaysPivotTable.PivotFields[Constants.PRIORITY], "", Spire.Xls.SubtotalTypes.Count);
                completedItems30DaysPivotTable.BuiltInStyle = Spire.Xls.PivotBuiltInStyles.PivotStyleMedium12;
                dashboardSheet.SetText(completedItems30DaysPivotTable.Location.Row - 1, completedItems30DaysPivotTable.Location.Column, Constants.COMPLETED_ITEMS_LAST_30_DAYS_SHEET);

                //// Completed items in a quarter.
                //Spire.Xls.Worksheet completedItemsInQuarterSheet = workbook.Worksheets[completedItemsInQuarter];
                //Spire.Xls.CellRange completedItemsInQuarterRange = completedItemsInQuarterSheet.Range[1, completedItemsInQuarterSheet.FirstColumn,
                //    completedItemsInQuarterSheet.LastRow - 1, completedItemsInQuarterSheet.LastColumn];
                //Spire.Xls.PivotCache completedItemsInQuarterCache = workbook.PivotCaches.Add(completedItemsInQuarterRange);
                //Spire.Xls.PivotTable completedItemsInQuarterPivotTable =
                //    dashboardSheet.PivotTables.Add(Constants.COMPLETED_ITEMS_QUARTER_SHEET,
                //    completedItems30DaysSheet.Range[completedItems30DaysPivotTable.Location.Row, completedItems30DaysPivotTable.Location.ColumnCount + 1], completedItemsInQuarterCache);
                //completedItemsInQuarterPivotTable.Options.ShowTooltips = false;
                //var r5 = completedItemsInQuarterPivotTable.PivotFields[Constants.STAKE_HOLDER];
                //r5.Axis = Spire.Xls.AxisTypes.Row;
                //completedItemsInQuarterPivotTable.Options.RowHeaderCaption = Constants.STAKE_HOLDER;
                //var r6 = completedItemsInQuarterPivotTable.PivotFields[Constants.PRIORITY];
                //r6.Axis = Spire.Xls.AxisTypes.Column;
                //completedItemsInQuarterPivotTable.Options.ColumnHeaderCaption = Constants.PRIORITY;
                //completedItemsInQuarterPivotTable.DataFields.Add(completedItemsInQuarterPivotTable.PivotFields[Constants.PRIORITY], "", Spire.Xls.SubtotalTypes.Count);
                //completedItemsInQuarterPivotTable.BuiltInStyle = Spire.Xls.PivotBuiltInStyles.PivotStyleMedium12;
                //dashboardSheet.SetText(completedItems30DaysPivotTable.Location.Row - 1,
                //    completedItems30DaysPivotTable.Location.ColumnCount + 1, string.Format(Constants.COMPLETED_ITEMS_QUARTER_SHEET, quarter));

                //// New Items last 7 days.
                //Spire.Xls.Worksheet newItemsLast7DaysSheet = workbook.Worksheets[newItemsLast7Days];
                //Spire.Xls.CellRange newItemsLast7DaysRange = newItemsLast7DaysSheet.Range[1, newItemsLast7DaysSheet.FirstColumn,
                //    newItemsLast7DaysSheet.LastRow - 1, newItemsLast7DaysSheet.LastColumn];
                //Spire.Xls.PivotCache newItemsLast7DaysCache = workbook.PivotCaches.Add(newItemsLast7DaysRange);
                //Spire.Xls.PivotTable newItemsLast7DaysPivotTable =
                //    dashboardSheet.PivotTables.Add(Constants.NEW_ITEMS_LAST_7_DAYS_SHEET,
                //    completedItems30DaysSheet.Range[completedItems30DaysPivotTable.Location.RowCount + 4, completedItems30DaysPivotTable.Location.Column], newItemsLast7DaysCache);
                //var r3 = newItemsLast7DaysPivotTable.PivotFields[Constants.STAKE_HOLDER];
                //r3.Axis = Spire.Xls.AxisTypes.Row;
                //newItemsLast7DaysPivotTable.Options.RowHeaderCaption = Constants.STAKE_HOLDER;
                //var r4 = newItemsLast7DaysPivotTable.PivotFields[Constants.PRIORITY];
                //r4.Axis = Spire.Xls.AxisTypes.Column;
                //newItemsLast7DaysPivotTable.Options.ColumnHeaderCaption = Constants.PRIORITY;
                //newItemsLast7DaysPivotTable.DataFields.Add(newItemsLast7DaysPivotTable.PivotFields[Constants.PRIORITY], "", Spire.Xls.SubtotalTypes.Count);
                //newItemsLast7DaysPivotTable.BuiltInStyle = Spire.Xls.PivotBuiltInStyles.PivotStyleMedium12;
                //dashboardSheet.SetText(completedItems30DaysPivotTable.Location.RowCount + 3, completedItems30DaysPivotTable.Location.Column, Constants.NEW_ITEMS_LAST_7_DAYS_SHEET);

                string destinationFileName = string.Format(@"IT Weekly Update {0} {1} {2}.xlsx", DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString());
                resultSheet = destinationFileName;
                //  workbook.Worksheets.Remove("Evaluation Warning";
                //
                workbook.SaveToFile(destinationFileName, Spire.Xls.ExcelVersion.Version2010);
                System.Diagnostics.Process.Start(destinationFileName);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                workbook.PivotCaches.Clear();
            }
        }
        string resultSheet = string.Empty;
        /// <summary>
        /// Copies the data from the downloaded excels to the current workbook.
        /// </summary>
        private void CopyDataToWorkbook()
        {
            try
            {
                string pathFileDestination = fileDetails;
                Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetDest = wbDest.Sheets[2];
                //Clear all contents in Destination workbook
                WorksheetDest.UsedRange.ClearContents();
                wbDest.Save();
                wbDest.Close();

                //Open the Source file
                Workbook wbSource = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["AllOpenItemsPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetSource = wbSource.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                Workbook wbDestination = excelApplication.Workbooks.Open(pathFileDestination);
                Worksheet WorksheetDestination = wbDestination.Sheets[2];
                WorksheetDestination.UsedRange.PasteSpecial(XlPasteType.xlPasteAll, XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
              
                wbSource.Close();

                //Open the Source file //C:\Users\kashas\Downloads\_Completed+Items+Last+30+Days+%28CCI+Project+Portal%29 (1).xls
                Workbook wbSource2 = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetSource2 = wbSource2.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource2.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                //Workbook wbDestination2= Workbooks.Open(pathFileDestination);
                Worksheet WorksheetDestination2 = wbDestination.Sheets[3];
                WorksheetDestination2.UsedRange.PasteSpecial(XlPasteType.xlPasteAll, XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
                wbSource2.Close();

                //Open the Source file
                Workbook wbSource3 = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["NewItemsLast7DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetSource3 = wbSource3.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource3.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                //Workbook wbDestination3 = Workbooks.Open(pathFileDestination);
                Worksheet WorksheetDestination3 = wbDestination.Sheets[4];
                WorksheetDestination3.UsedRange.PasteSpecial(XlPasteType.xlPasteAll, XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
                wbSource3.Close();

                //Open the Source file
                Workbook wbSource4 = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["CompletedItemsInaQuarterPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetSource4 = wbSource4.Sheets[1];
                //Copy all range in this worksheet
                WorksheetSource4.UsedRange.Copy(Missing.Value);
                //Open destination workbook
                //Workbook wbDestination4 = Workbooks.Open(pathFileDestination);
                Worksheet WorksheetDestination4 = wbDestination.Sheets[5];
                WorksheetDestination4.UsedRange.PasteSpecial(XlPasteType.xlPasteAll, XlPasteSpecialOperation.xlPasteSpecialOperationNone);
                wbDestination.Save();
                wbSource4.Close();

                wbDestination.Close();
                excelApplication.Quit();
            }

            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Add the worksheets.
        /// </summary>
        private void AddWorkSheets()
        {
            try
            {
                var sheetCollection = new Microsoft.Office.Interop.Excel.Worksheet[5];
                sheetCollection[0] = workBook.Worksheets.Add();
                sheetCollection[0].Name = string.Format(Constants.COMPLETED_ITEMS_QUARTER_SHEET, quarter);
                sheetCollection[1] = workBook.Worksheets.Add();
                sheetCollection[1].Name = Constants.NEW_ITEMS_LAST_7_DAYS_SHEET;
                sheetCollection[2] = workBook.Worksheets.Add();
                sheetCollection[2].Name = Constants.COMPLETED_ITEMS_LAST_30_DAYS_SHEET;
                sheetCollection[3] = workBook.Worksheets.Add();
                sheetCollection[3].Name = Constants.ALL_OPEN_ITEMS_SHEET;
                sheetCollection[4] = workBook.Worksheets.Add();
                sheetCollection[4].Name = Constants.DASHBOARD_SHEET;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Saves the excel file in the specified name format.
        /// </summary>
        private void SaveExcelFile()
        {
            try
            {
                string currentYear = DateTime.Now.Year.ToString();
                string currentMonth = DateTime.Now.Month.ToString();
                string currentDate = DateTime.Now.Day.ToString();
                var appSettings = ConfigurationManager.AppSettings;
                string filePath = appSettings["FilePath"];
                fileDetails = string.Format(@"{0} IT Weekly Update {1} {2} {3}.xlsx", filePath, currentYear, currentMonth, currentDate);
                this.excelApplication.ActiveWorkbook.SaveAs(fileDetails);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void SendMail()
        {
            try
            {
                // Create the Outlook application.
                Outlook.Application oApp = new Outlook.Application();
                // Create a new mail item.
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                oMsg.HTMLBody = "Hi,"+ System.Environment.NewLine +" PFA the weekly report.";
                //Add an attachment.
                String sDisplayName = "MyAttachment";
                int iPosition = (int)oMsg.Body.Length + 1;
                int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
                string excelPath = string.Format(ConfigurationManager.AppSettings["WeeklyReportPath"], resultSheet);
                Outlook.Attachment oAttach = oMsg.Attachments.Add(excelPath, iAttachType, iPosition, sDisplayName);
                //Subject line
                oMsg.Subject = "IT Weekly Update";
                // Add a recipient.
                Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;
                Outlook.Recipient oRecip = null;
                string recipientList = ConfigurationManager.AppSettings["Recipients"];
                string[] recipient = recipientList.Split(';');
                for (int i = 0; i < recipient.Length; i++)
                     oRecip = (Outlook.Recipient)oRecips.Add(recipient[i]);
                
                oRecip.Resolve();
                // Send.
                oMsg.Send();
                // Clean up.
                oRecip = null;
                oRecips = null;
                oMsg = null;
                oApp = null;
            }
            catch (Exception exception)
            {
                throw exception;
            }//end 
        }

        static readonly object useDefault = Type.Missing;
        //PivotTable pivotTable;
        //Range pivotData;
        //Range pivotDestination;
        //PivotField descriptionPivotField;
        //PivotField shortnamePivotField;
        //PivotField itemcodePivotField;
        //PivotField pricePivotField;
        //string pivotTableName = @"SupercalifornialisticXPalbeauxdacious";
        private void AddPivotTable()
        {
            try
            {
                PivotTable pivotTable;
                Range pivotData;
                Range pivotDestination;
                PivotField descriptionPivotField;
                PivotField shortnamePivotField;
                PivotField itemcodePivotField;
                PivotField pricePivotField;
                string pivotTableName = @"SupercalifornialisticXPalbeauxdacious";
                //         <add key="CompletedItemsLast30DaysPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\Completed Items Last 30 Days (CCI) (1).csv"/>
                //<add key="CompletedItemsInaQuarterPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\Completed Items Q1 2017 (CCI) (1).csv"/>
                //<add key="NewItemsLast7DaysPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\New Items Last 7 Days (CCI) (1).csv"/>-->
                //<!--<add key ="AllOpenItemsPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\_All+Open+Items+(CCI+Project+Portal).xls"/>
                //<add key="CompletedItemsLast30DaysPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\_Completed+Items+Last+30+Days+(CCI+Project+Portal).xls"/>
                //    string filePath = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\_Completed+Items+Last+30+Days+(CCI+Project+Portal).xls";
                  //string filePath = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\Completed within 30 days (CCI) - Copy.csv";

            //    string filePath = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\ IT Weekly Update 2017 4 5.xlsx";
                //Workbook _xlBook = excelApplication.Workbooks.Open(filePath, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                List<string> excelPathList = new List<string>();
              string file  = ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"];
              excelPathList.Add(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"]);
              excelPathList.Add(ConfigurationManager.AppSettings["NewItemsLast7DaysPath"]);
            //  for (int i = 0; i < excelPathList.Count; i++)
              {
                  string cellName;
                ////  if (i == 0)
                //      cellNaeme = "A2";
                //  else
                //      cellName = "A25";

                  Workbook _xlBook = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                  Worksheet _xlSheet = _xlBook.Sheets[1];

                  string pathFileDestination = fileDetails;
                  Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                  Worksheet WorksheetDest = wbDest.Sheets[1];
                
                  int inc = 1;
                 // string pivotTableName;
                //  if (i == 0)
                      pivotTableName = @"SupercalifornialisticXPalbeauxdacious";
                  //else
                  //    pivotTableName = @"Pivo Table2";
                  pivotData = _xlSheet.UsedRange;
                  // pivotData = _xlSheet.Range["A1:M25"];
                  //     pivotData  = _xlSheet.Range[];
                  pivotDestination = WorksheetDest.Range["A2", useDefault];
                  WorksheetDest.PivotTableWizard(
                          XlPivotTableSourceType.xlDatabase,
                          pivotData,
                          pivotDestination,
                          pivotTableName,
                          true,
                          true,
                          true,
                          true,
                          useDefault,
                          useDefault,
                          false,
                          false,
                          XlOrder.xlDownThenOver,
                          0,
                          useDefault,
                          useDefault
                  );

                  // Set variables used to manipulate the Pivot Table.
                  pivotTable = (PivotTable)WorksheetDest.PivotTables(pivotTableName);

                  //shortnamePivotField = (PivotField)pivotTable.PivotFields(2);
                  //itemcodePivotField = (PivotField)pivotTable.PivotFields(3);
                  //descriptionPivotField = (PivotField)pivotTable.PivotFields(4);
                  //pricePivotField = (PivotField)pivotTable.PivotFields(7);

                  shortnamePivotField = (PivotField)pivotTable.PivotFields(1);
                  itemcodePivotField = (PivotField)pivotTable.PivotFields(5);
                  //descriptionPivotField = (PivotField)pivotTable.PivotFields(4);
                  //pricePivotField = (PivotField)pivotTable.PivotFields(7);

                  // Format the Pivot Table.
                  pivotTable.Format(XlPivotFormatType.xlReport2);
                  pivotTable.InGridDropZones = false;
                  pivotTable.SmallGrid = false;
                  pivotTable.ShowTableStyleRowStripes = true;
                  pivotTable.TableStyle2 = "PivotStyleLight1";

                  //// Page Field
                  //shortnamePivotField.Orientation = XlPivotFieldOrientation.xlPageField;
                  //shortnamePivotField.Position = 1;
                  //shortnamePivotField.CurrentPage = "(All)";

                  //// Row Fields
                  //descriptionPivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                  //descriptionPivotField.Position = 1;
                  //itemcodePivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                  //itemcodePivotField.Position = 2;

                  //// Data Field
                  //pricePivotField.Orientation = XlPivotFieldOrientation.xlDataField;
                  //pricePivotField.Function = XlConsolidationFunction.xlSum;


                  shortnamePivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                  shortnamePivotField.Position = 1;
                  //shortnamePivotField.CurrentPage = "(All)";

                  // Row Fields
                  //descriptionPivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                  //descriptionPivotField.Position = 1;
                  itemcodePivotField.Orientation = XlPivotFieldOrientation.xlColumnField;
                  itemcodePivotField.Position = 1;

                  // Data Field
                  itemcodePivotField.Orientation = XlPivotFieldOrientation.xlDataField;
                  itemcodePivotField.Function = XlConsolidationFunction.xlSum;

                  wbDest.Save();
              }
            }
            catch (Exception)
            {
                
                throw;
            }
           

        }

        private void AddPivotTable2()
        {
            try
            {
                PivotTable pivotTable;
                Range pivotData;
                Range pivotDestination;
                PivotField descriptionPivotField;
                PivotField shortnamePivotField;
                PivotField itemcodePivotField;
                PivotField pricePivotField;
                string pivotTableName = @"Pivot Table2";
                List<string> excelPathList = new List<string>();
                string file = ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"];
                excelPathList.Add(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"]);
                excelPathList.Add(ConfigurationManager.AppSettings["NewItemsLast7DaysPath"]);
                //  for (int i = 0; i < excelPathList.Count; i++)
                {
                    string cellName;
                    ////  if (i == 0)
                    //      cellName = "A2";
                    //  else
                    //      cellName = "A25";

                    Workbook _xlBook = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["NewItemsLast7DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                    Worksheet _xlSheet = _xlBook.Sheets[1];

                    string pathFileDestination = fileDetails;
                    Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                    Worksheet WorksheetDest = wbDest.Sheets[1];

                    int inc = 1;
                   // string pivotTableName;
                    //  if (i == 0)
                    pivotTableName = @"pivot table 2";
                    //else
                    //    pivotTableName = @"Pivo Table2";
                    pivotData = _xlSheet.UsedRange;
                    // pivotData = _xlSheet.Range["A1:M25"];
                    //     pivotData  = _xlSheet.Range[];
                    pivotDestination = WorksheetDest.Range["A30", useDefault];
                    WorksheetDest.PivotTableWizard(
                            XlPivotTableSourceType.xlDatabase,
                            pivotData,
                            pivotDestination,
                            pivotTableName,
                            true,
                            true,
                            true,
                            true,
                            useDefault,
                            useDefault,
                            false,
                            false,
                            XlOrder.xlDownThenOver,
                            0,
                            useDefault,
                            useDefault
                    );

                    // Set variables used to manipulate the Pivot Table.
                    pivotTable = (PivotTable)WorksheetDest.PivotTables(pivotTableName);

                    //shortnamePivotField = (PivotField)pivotTable.PivotFields(2);
                    //itemcodePivotField = (PivotField)pivotTable.PivotFields(3);
                    //descriptionPivotField = (PivotField)pivotTable.PivotFields(4);
                    //pricePivotField = (PivotField)pivotTable.PivotFields(7);

                    shortnamePivotField = (PivotField)pivotTable.PivotFields(1);
                    itemcodePivotField = (PivotField)pivotTable.PivotFields(5);
                    //descriptionPivotField = (PivotField)pivotTable.PivotFields(4);
                    //pricePivotField = (PivotField)pivotTable.PivotFields(7);

                    // Format the Pivot Table.
                    pivotTable.Format(XlPivotFormatType.xlReport2);
                    pivotTable.InGridDropZones = false;
                    pivotTable.SmallGrid = false;
                    pivotTable.ShowTableStyleRowStripes = true;
                    pivotTable.TableStyle2 = "PivotStyleLight1";

                    //// Page Field
                    //shortnamePivotField.Orientation = XlPivotFieldOrientation.xlPageField;
                    //shortnamePivotField.Position = 1;
                    //shortnamePivotField.CurrentPage = "(All)";

                    //// Row Fields
                    //descriptionPivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                    //descriptionPivotField.Position = 1;
                    //itemcodePivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                    //itemcodePivotField.Position = 2;

                    //// Data Field
                    //pricePivotField.Orientation = XlPivotFieldOrientation.xlDataField;
                    //pricePivotField.Function = XlConsolidationFunction.xlSum;


                    shortnamePivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                    shortnamePivotField.Position = 1;
                    //shortnamePivotField.CurrentPage = "(All)";

                    // Row Fields
                    //descriptionPivotField.Orientation = XlPivotFieldOrientation.xlRowField;
                    //descriptionPivotField.Position = 1;
                    itemcodePivotField.Orientation = XlPivotFieldOrientation.xlColumnField;
                    itemcodePivotField.Position = 1;

                    // Data Field
                    itemcodePivotField.Orientation = XlPivotFieldOrientation.xlDataField;
                    itemcodePivotField.Function = XlConsolidationFunction.xlSum;

                    wbDest.Save();
                }
            }
            catch (Exception)
            {

                throw;
            }


        }

        //private void AddPivotTable(Worksheet _xlSheet, Worksheet WorksheetDest)
        //{
        //    try
        //    {
        //        //         <add key="CompletedItemsLast30DaysPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\Completed Items Last 30 Days (CCI) (1).csv"/>
        //        //<add key="CompletedItemsInaQuarterPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\Completed Items Q1 2017 (CCI) (1).csv"/>
        //        //<add key="NewItemsLast7DaysPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\New Items Last 7 Days (CCI) (1).csv"/>-->
        //        //<!--<add key ="AllOpenItemsPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\_All+Open+Items+(CCI+Project+Portal).xls"/>
        //        //<add key="CompletedItemsLast30DaysPath" value="C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\_Completed+Items+Last+30+Days+(CCI+Project+Portal).xls"/>
        //        //    string filePath = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\_Completed+Items+Last+30+Days+(CCI+Project+Portal).xls";
        //        //string filePath = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\Completed within 30 days (CCI) - Copy.csv";

        //        //    string filePath = @"C:\Sneha\Demo Projects\WeeklyReportDemo\test sheets\ IT Weekly Update 2017 4 5.xlsx";
        //        //Workbook _xlBook = excelApplication.Workbooks.Open(filePath, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
        //       // Workbook _xlBook = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
        //      //  Worksheet _xlSheet = _xlBook.Sheets[1];

        //        //string pathFileDestination = fileDetails;
        //        //Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
        //        //Worksheet WorksheetDest = wbDest.Sheets[1];
        //        pivotData = _xlSheet.UsedRange;
        //        pivotDestination = WorksheetDest.Range["A2", useDefault];
        //        WorksheetDest.PivotTableWizard(
        //                XlPivotTableSourceType.xlDatabase,
        //                pivotData,
        //                pivotDestination,
        //                pivotTableName,
        //                true,
        //                true,
        //                true,
        //                true,
        //                useDefault,
        //                useDefault,
        //                false,
        //                false,
        //                XlOrder.xlDownThenOver,
        //                0,
        //                useDefault,
        //                useDefault
        //        );

        //        // Set variables used to manipulate the Pivot Table.
        //        pivotTable = (PivotTable)WorksheetDest.PivotTables(pivotTableName);

        //        shortnamePivotField = (PivotField)pivotTable.PivotFields(1);
        //        itemcodePivotField = (PivotField)pivotTable.PivotFields(5);

        //        // Format the Pivot Table.
        //        pivotTable.Format(XlPivotFormatType.xlReport2);
        //        pivotTable.InGridDropZones = false;
        //        pivotTable.SmallGrid = false;
        //        pivotTable.ShowTableStyleRowStripes = true;
        //        pivotTable.TableStyle2 = "PivotStyleLight1";

        //        // Row Fields
        //        shortnamePivotField.Orientation = XlPivotFieldOrientation.xlRowField;
        //        shortnamePivotField.Position = 1;

                
        //        itemcodePivotField.Orientation = XlPivotFieldOrientation.xlColumnField;
        //        itemcodePivotField.Position = 1;

        //        // Data Field
        //        itemcodePivotField.Orientation = XlPivotFieldOrientation.xlDataField;
        //        itemcodePivotField.Function = XlConsolidationFunction.xlSum;

        //    //    wbDest.Save();
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }


        //}


        public void CPV()
        {
            try
            {
                PivotTable pivotTable = null;
                Range pivotData;
                Range pivotDestination;
                PivotField rowField = null;
                PivotCache pivotCache = null;
                PivotField columnField = null ;
                string pivotTableName = "PivotTable1";

                // Open the 30 days sheet
                Workbook _xlBook = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet _xlSheet = _xlBook.Sheets[1];

                // Open the pivot Destination file.
                string pathFileDestination = fileDetails;
                Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetDest = wbDest.Sheets[1];

                pivotData = _xlSheet.UsedRange;
                pivotDestination = WorksheetDest.Range["A2", useDefault];

                WorksheetDest.PivotTableWizard(XlPivotTableSourceType.xlDatabase,pivotData,pivotDestination,pivotTableName,true,true,true,true,useDefault,useDefault,false,false,XlOrder.xlDownThenOver,0,useDefault,useDefault);

                PivotTable pivotTable2;
                Range pivotData2;
                Range pivotDestination2;
                PivotField rowField2; 
                PivotField columnField2;
                string pivotTableName2 = "PivotTable2";

                // Open the 30 days sheet
                Workbook _xlBook2 = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["NewItemsLast7DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet _xlSheet2 = _xlBook2.Sheets[1];

                pivotData2 = _xlSheet2.UsedRange;
                pivotDestination2 = WorksheetDest.Range["A25", useDefault];
                WorksheetDest.PivotTableWizard(XlPivotTableSourceType.xlDatabase, pivotData2, pivotDestination2, pivotTableName2, true, true, true, true, useDefault, useDefault, false, false, XlOrder.xlDownThenOver, 0, useDefault, useDefault);

                // Set variables used to manipulate the Pivot Table.
                
                pivotTable2 = (PivotTable)WorksheetDest.PivotTables(pivotTableName2);
                pivotTable = (PivotTable)WorksheetDest.PivotTables(pivotTableName);
                rowField = (PivotField)pivotTable.PivotFields(1);
                columnField = (PivotField)pivotTable.PivotFields(5);

                rowField2 = (PivotField)pivotTable2.PivotFields(1);
                columnField2 = (PivotField)pivotTable2.PivotFields(5);

                // Format the Pivot Table.
                pivotTable.Format(XlPivotFormatType.xlReport2);
                pivotTable.InGridDropZones = false;
                pivotTable.SmallGrid = false;
                pivotTable.ShowTableStyleRowStripes = true;
                pivotTable.TableStyle2 = "PivotStyleLight1";

                pivotTable2.Format(XlPivotFormatType.xlReport2);
                pivotTable2.InGridDropZones = false;
                pivotTable2.SmallGrid = false;
                pivotTable2.ShowTableStyleRowStripes = true;
                pivotTable2.TableStyle2 = "PivotStyleLight1";

                // Row Fields
                rowField.Orientation = XlPivotFieldOrientation.xlRowField;
                rowField.Position = 1;

                rowField2.Orientation = XlPivotFieldOrientation.xlRowField;
                rowField2.Position = 1;

                // Column Field
                columnField.Orientation = XlPivotFieldOrientation.xlColumnField;
                columnField.Position = 1;

                columnField2.Orientation = XlPivotFieldOrientation.xlColumnField;
                columnField2.Position = 1;

                columnField.Orientation = XlPivotFieldOrientation.xlDataField;
                columnField.Function = XlConsolidationFunction.xlSum;

                columnField2.Orientation = XlPivotFieldOrientation.xlDataField;
                columnField2.Function = XlConsolidationFunction.xlSum;
                
                wbDest.Save();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void NewCPV_2()
        {
            try
            {
                // Open the pivot Destination file.
                string pathFileDestination = fileDetails;
                Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetDest = wbDest.Sheets[1];

                List<string> workbookList = new List<string>();
                workbookList.Add(ConfigurationManager.AppSettings["AllOpenItemsPath"]);
                workbookList.Add(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"]);
                workbookList.Add(ConfigurationManager.AppSettings["NewItemsLast7DaysPath"]);
                for (int i = 0; i < workbookList.Count; i++)
                {
                    PivotTable pivotTable = null;
                    Range pivotData;
                    Range pivotDestination;
                    PivotField rowField = null;
                    PivotField columnField = null;
                    PivotField filters = null;
                    string pivotTableName = String.Format("PivotTable {0}", (i + 1).ToString());

                  //  Workbook _xlBook = excelApplication.Workbooks.Open(workbookList[i], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                    Worksheet _xlSheet = wbDest.Sheets[i + 2];
                    pivotData = _xlSheet.UsedRange;
                    string excelRange = "A3";
                    if (i == 0)
                        excelRange = String.Format("A3");
                    else if (i == 1)
                        excelRange = " R14";
                    else
                        excelRange = "A50";
                    
                    pivotDestination = WorksheetDest.Range[excelRange, useDefault];

                    WorksheetDest.PivotTableWizard(XlPivotTableSourceType.xlDatabase, pivotData, pivotDestination, pivotTableName, true, true, true, true, useDefault, useDefault, false, false, XlOrder.xlDownThenOver, 0, useDefault, useDefault);
                    // Set variables used to manipulate the Pivot Table.
                    pivotTable = (PivotTable)WorksheetDest.PivotTables(pivotTableName);

                    rowField = (PivotField)pivotTable.PivotFields(1);
                    columnField = (PivotField)pivotTable.PivotFields(5);
                    filters = (PivotField)pivotTable.PivotFields(6);
                    // pageField = (PivotField)pivotTable.PivotFields(6);
                    // Format the Pivot Table.
                    pivotTable.Format(XlPivotFormatType.xlReport2);
                    pivotTable.InGridDropZones = false;
                    pivotTable.SmallGrid = false;
                    pivotTable.ShowTableStyleRowStripes = true;
                    pivotTable.TableStyle2 = "PivotStyleLight1";

                    filters.Orientation = XlPivotFieldOrientation.xlPageField;
                    filters.Position = 1;
                    filters.CurrentPage = "(All)";

                    // Row Fields
                    rowField.Orientation = XlPivotFieldOrientation.xlRowField;
                    rowField.Position = 1;

                    // Column Field
                    columnField.Orientation = XlPivotFieldOrientation.xlColumnField;
                    columnField.Position = 1;

                    columnField.Orientation = XlPivotFieldOrientation.xlDataField;
                    columnField.Function = XlConsolidationFunction.xlCount;

                }
                wbDest.Save();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public void NewCPV()
        {
            try
            {
                // Open the pivot Destination file.
                string pathFileDestination = fileDetails;
                Workbook wbDest = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                Worksheet WorksheetDest = wbDest.Sheets[1];

                List<string> workbookList = new List<string>();
                workbookList.Add(ConfigurationManager.AppSettings["AllOpenItemsPath"]);
                workbookList.Add(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"]);
                workbookList.Add(ConfigurationManager.AppSettings["NewItemsLast7DaysPath"]);
              //  int j = 6;
                for (int i = 0; i < workbookList.Count-2; i++)
                {
                   
                    //WorksheetDest = wbDest.Sheets[j];
                    //j++;
                    PivotTable pivotTable = null;
                    Range pivotData;
                    Range pivotDestination;
                    PivotField rowField = null;
                    PivotField columnField = null;
                    PivotField filters = null;
                    string pivotTableName = String.Format("PivotTable1");


                    Workbook _xlBook = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["AllOpenItemsPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                    Worksheet _xlSheet = _xlBook.Sheets[1];
                    pivotData = _xlSheet.UsedRange;
                    string excelRange = "A3";
                    //if (i == 0)
                    //    excelRange = String.Format("A3");
                    //else if (i == 1)
                    //    excelRange = "H14";
                    //else
                    //    excelRange = "A150";

                    pivotDestination = WorksheetDest.Range[excelRange, useDefault];

                    WorksheetDest.PivotTableWizard(XlPivotTableSourceType.xlDatabase, pivotData, pivotDestination, pivotTableName, true, true, true, true, useDefault, useDefault, false, false, XlOrder.xlDownThenOver, 0, useDefault, useDefault);
                    // Set variables used to manipulate the Pivot Table.
                    pivotTable = (PivotTable)WorksheetDest.PivotTables(pivotTableName);
                    
                    rowField = (PivotField)pivotTable.PivotFields(1);
                    columnField = (PivotField)pivotTable.PivotFields(5);
                    filters = (PivotField)pivotTable.PivotFields(6);
                   // pageField = (PivotField)pivotTable.PivotFields(6);
                    // Format the Pivot Table.
                    pivotTable.Format(XlPivotFormatType.xlReport2);
                    pivotTable.InGridDropZones = false;
                    pivotTable.SmallGrid = false;
                    pivotTable.ShowTableStyleRowStripes = true;
                    pivotTable.TableStyle2 = "PivotStyleLight1";

                    filters.Orientation = XlPivotFieldOrientation.xlRowField;
                    filters.Position =1;
                   // filters.CurrentPage = "(All)";

                    // Row Fields
                    rowField.Orientation = XlPivotFieldOrientation.xlPageField; 
                    rowField.Position = 1;
                    

                    // Column Field
                    columnField.Orientation = XlPivotFieldOrientation.xlColumnField;
                    columnField.Position = 1;

                    columnField.Orientation = XlPivotFieldOrientation.xlDataField;
                    columnField.Function = XlConsolidationFunction.xlCount;
                    
                }
                 wbDest.Save();
                 wbDest.Close();
                 {
                     Workbook wbDest1 = excelApplication.Workbooks.Open(pathFileDestination, 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                     Worksheet WorksheetDest1 = wbDest1.Sheets[1];
                     //WorksheetDest = wbDest.Sheets[j];
                     //j++;
                     PivotTable pivotTable1 = null;
                     Range pivotData1;
                     Range pivotDestination1;
                     PivotField rowField1 = null;
                     PivotField columnField1 = null;
                     PivotField filters1 = null;
                     string pivotTableName1 = String.Format("PivotTable1");


                     Workbook _xlBook1 = excelApplication.Workbooks.Open(ConfigurationManager.AppSettings["CompletedItemsLast30DaysPath"], 0, false, 1, "", "", false, XlPlatform.xlWindows, 9, true, false, 0, true, false, false);
                     Worksheet _xlSheet1 = _xlBook1.Sheets[1];
                     pivotData1 = _xlSheet1.UsedRange;
                     string excelRange1 = "A3";
                     //if (i == 0)
                     //    excelRange = String.Format("A3");
                     //else if (i == 1)
                     //    excelRange = "H14";
                     //else
                     //    excelRange = "A150";

                     pivotDestination1 = WorksheetDest1.Range[excelRange1, useDefault];

                     WorksheetDest1.PivotTableWizard(XlPivotTableSourceType.xlDatabase, pivotData1, pivotDestination1, pivotTableName1, true, true, true, true, useDefault, useDefault, false, false, XlOrder.xlDownThenOver, 0, useDefault, useDefault);
                     // Set variables used to manipulate the Pivot Table.
                     pivotTable1 = (PivotTable)WorksheetDest1.PivotTables(pivotTableName1);

                     rowField1 = (PivotField)pivotTable1.PivotFields(1);
                     columnField1 = (PivotField)pivotTable1.PivotFields(5);
                     filters1 = (PivotField)pivotTable1.PivotFields(6);
                     // pageField = (PivotField)pivotTable.PivotFields(6);
                     // Format the Pivot Table.
                     pivotTable1.Format(XlPivotFormatType.xlTable6);
                     pivotTable1.InGridDropZones = false;
                     pivotTable1.SmallGrid = false;
                     pivotTable1.ShowTableStyleRowStripes = true;
                     pivotTable1.TableStyle2 = "PivotStyleLight1";

                     filters1.Orientation = XlPivotFieldOrientation.xlRowField;
                     filters1.Position = 1;
                     // filters.CurrentPage = "(All)";

                     // Row Fields
                     rowField1.Orientation = XlPivotFieldOrientation.xlPageField;
                     rowField1.Position = 1;


                     // Column Field
                     columnField1.Orientation = XlPivotFieldOrientation.xlColumnField;
                     columnField1.Position = 1;

                     columnField1.Orientation = XlPivotFieldOrientation.xlDataField;
                     columnField1.Function = XlConsolidationFunction.xlCount;
                     wbDest1.Save();
                     wbDest1.Close();
                 }
               
                
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        #endregion
    }
}
