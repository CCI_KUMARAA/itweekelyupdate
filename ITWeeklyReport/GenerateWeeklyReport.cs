﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spire.Xls;

namespace ITWeeklyReport
{
    public class GenerateWeeklyReport
    {
        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public GenerateWeeklyReport()
        {
            // Creating excel document.
            GetPivot();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates the Pivot table
        /// </summary>
        public void GetPivot()
        {
            try
            {
                Workbook workbook = new Workbook();
                string fileName = ConfigurationManager.AppSettings["FilePath"];
                string openItemsSheet = ConfigurationManager.AppSettings["CompletedItems30"];
                string dashboard = ConfigurationManager.AppSettings["Dashboard"];

                workbook.LoadFromFile(fileName);               
                
                Worksheet sheet = workbook.Worksheets[openItemsSheet];
                Worksheet sheet2 = workbook.Worksheets[dashboard];                

                CellRange dataRange = sheet.Range["A1:M3000"];
                PivotCache cache = workbook.PivotCaches.Add(dataRange);
                PivotTable pt = sheet2.PivotTables.Add("Pivot Table", sheet.Range["A1"], cache);

                var r1 = pt.PivotFields["Stake Holder"];
                r1.Axis = AxisTypes.Row;
                pt.Options.RowHeaderCaption = "Stake Holder";

                var r2 = pt.PivotFields["Priority"];
                r2.Axis = AxisTypes.Column;
                pt.Options.RowHeaderCaption = "Priority";

     
                pt.DataFields.Add(pt.PivotFields["Priority"], "", SubtotalTypes.Count);
               
              
                pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleMedium12;

                workbook.SaveToFile("PivotTable.xlsx", ExcelVersion.Version2010);
                System.Diagnostics.Process.Start("PivotTable.xlsx");
            }
            catch (Exception ex)
            { 

            }
        }

        #endregion

    }
}
